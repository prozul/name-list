import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './index.css';
import logo from './gladiator.png';

class FetchDemo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      loading: true,
      error: null
    };
  }

  componentDidMount() {
    axios.get(`http://localhost:8000/names.php`)
      .then(res => {
        console.log(res);
        // Transform the raw data by extracting the nested posts
        const posts = res.data.map(obj => obj);

        // Update state to trigger a re-render.
        // Clear any errors, and turn off the loading indiciator.
        this.setState({
          posts,
          loading: false,
          error: null
        });
      })
      .catch(err => {
        // Something went wrong. Save the error in state and re-render.
        this.setState({
          loading: false,
          error: err
        });
      });
  }

  renderLoading() {
    return <div>Loading...</div>;
  }

  renderError() {
    return (
      <div>
        Uh oh: {this.state.error.message}
      </div>
    );
  }

  renderPosts() {
    if(this.state.error) {
      return this.renderError();
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">This is BAE AI's Gladiators</h1>
        </header>
        {this.state.posts.map(post =>
          <div>{post}</div>
        )}
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.state.loading ?
          this.renderLoading()
          : this.renderPosts()}
      </div>
    );
  }
}

// Change the subreddit to anything you like
ReactDOM.render(
  <FetchDemo subreddit="Gladiators"/>,
  document.getElementById('root')
);
